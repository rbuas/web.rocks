WebPigeonTarget = (function(socketio) {

    function WebPigeonTarget (options) {
        var self = this;
        self.socket = socketio();
        self.badget = null;
        if(options.badget) self.connect(options.badget);

        if(options.targets) self.targets(options.targets);
        if(options.broadcast) self.targets(options.broadcast);
        if(options.broadcast) self.broadcast(options.broadcast);
    }

    WebPigeonTarget.prototype.connect = function(badget, callback) {
        var self = this;
        if(self.badget) self.disconnect();

        self.badget = badget;
        self.socket.emit("CONNECT", self.badget, callback);
    }

    WebPigeonTarget.prototype.disconnect = function(badget, callback) {
        var self = this;
        if(!self.badget) return;

        self.badget = null;
        self.socket.emit("DISCONNECT", callback);
    }

    WebPigeonTarget.prototype.targets = function(callback) {
        var self = this;
        self.socket.on("TARGETS", callback);
    }

    WebPigeonTarget.prototype.broadcast = function(callback) {
        var self = this;
        self.socket.on("BROADCAST", callback);
    }

    WebPigeonTarget.prototype.targetcast = function(callback) {
        var self = this;
        self.socket.on("TARGETCAST", callback);
    }



    // PRIVATE
    function isMyself(self, badget) {
        return self.badget && badget && self.badget.id === badget.id && self.badget.sid === badget.sid;
    }

    return WebPigeonTarget;
})(io);